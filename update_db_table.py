import MySQLdb

table = "database_table"
mysql_credential = "username", "password", "database"

newstuff, output = [], []
id = 1

try:
	db = MySQLdb.connect(user="{0[0]}".format(mysql_credential), passwd="{0[1]}".format(mysql_credential), 
						 db="{0[2]}".format(mysql_credential))
except MySQLdb.OperationalError:
	print('Inconsistent database\credentials, cannot proceed')
	quit()
cursor = db.cursor()
query_pull = "SELECT * FROM {0} order by DATE".format(table)
cursor.execute(query_pull)
for row in cursor:
	output.append([int(row[0]), int(row[1]), float(row[2]), float(row[3])])

for key, element in enumerate(output):
	newstuff.append([float(str(id)), int(output[key][1]), float(output[key][2]), float(output[key][3])])
	id += 0.1

query_db_create = """CREATE TABLE IF NOT EXISTS `{0}_sorted` (`ID` float(11) NOT NULL,  
					`DATE` int(13) NOT NULL,  `PRICE` float NOT NULL,  `AMOUNT` float NOT NULL,   
					PRIMARY KEY (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;""".format(table)

cursor.execute(query_db_create)
db.commit()

for (offset, item) in enumerate(newstuff):
	query_push="""INSERT INTO {0}_sorted (ID, DATE, PRICE, AMOUNT) VALUES
	({1}, {2}, {3}, {4})""".format(table, float(newstuff[offset][0]), newstuff[offset][1],
	float(newstuff[offset][2]), float(newstuff[offset][3]))

	cursor.execute(query_push)
	db.commit()

query_db_rename1 = "RENAME TABLE `datmarket`.`{0}` TO `datmarket`.`{0}_old` ;".format(table)
cursor.execute(query_db_rename1)
db.commit()

query_db_rename2 = "RENAME TABLE `datmarket`.`{0}_sorted` TO `datmarket`.`{0}` ;".format(table)
cursor.execute(query_db_rename2)
db.commit()
cursor.close()